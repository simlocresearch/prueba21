#ifndef Modelo_h
#define Modelo_h

#pragma once
#include <string>
#include <math.h>
#include <stdio.h>

// La clase Modelo permite recrear la respuesta de un sistema LTI de hasta 
// 2 ceros y 2 polos ante una entrada escalon u(t) de amplitud K
// Y(S)=k·(tZ1·s+1)·(tZ2·s+1)
//        ---------------
//        (tP1·s+1)·(tP2·s+1)

class Modelo
{
public:
    Modelo(float cteT);
    virtual ~Modelo();
    void StartModelo(float _t,float ,float);//Inicia el sistema LTI
    bool ModeloStarted();//Acceso al estado ON/OFF del sistema LTI
    void Update(float t);//Calcular salida del sistema LTI y(t)                  ______
    bool Check_K(float _t,float new_k);//Comprueba cambios en la señal de consigna K __|
    float GetValue();
     
private:
    const std::string m_name;//Nombre del instrumento
    float k,aux_k;//Amplitud de la señal de consigna Escalon
    float tZ1,tZ2,tP1,tP2;//Constantes del sistema LTI Y(S)
    float c1,c2,c3,c4;//Constantes del sistema LTI y(t)
    float c0;//Condicion inicial
    bool subamortiguado;//Polos complejos
    float c5;//Constantes del sistema LTI y(t) subamortiguado
    float t0,t; //Tiempo del sistema LTI y(t)
    bool state;//Estado ON/OFF del sistema LTI
    float value;//Salida del sistema y(t)
    bool permanente;//Estado permanente del sistema LTI
};
#endif
