#SIMLOR SW DEPARTMENT 2017
#Script para construir y ejecutar proyecto
echo "*** Limpiando restos de compilación ***"
rm cmake_install.cmake CMakeCache.txt Makefile
rm -r CMakeFiles
rm -r bin
make clean
mkdir bin
echo "*** Interpretando CMakeLists.txt  ***"
cmake .
echo "*** Interpretando MakeFile  ***"
make
echo "*** Lanzando aplicación  ***"
./bin/a320pedales
