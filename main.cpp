//Librerias
#include <stdio.h>
#include <unistd.h>
#include <curses.h>
#include <ctime>
#include <stdlib.h>
#include <iostream>

//Dependencias locales
#include "PedalDevice.h"
#include "Gauge.h"
#include "Modelo.h"

//Cabeceras funciones
void* hilo_tiempo(void* d); //Hilo de control temporal

//Variables globales
double tiempo=0.0; //Tiempo transcurrido del programa en seg
int tecla; //Tecla pulsada

int main()
{
	//Hilo de temporización
	pthread_t thid;
	pthread_create(&thid, 0 ,hilo_tiempo, NULL);
    
    // Inicialización de ncurses	
	initscr();
	start_color();
    cbreak();
    noecho();
    curs_set( 0 );
    nodelay( stdscr, TRUE );
	keypad( stdscr, TRUE );

    //Objeto de comunicación con periférico
    PedalDevice devPedal;
    //Objeto de representación del instrumento
    Gauge gauPedal;
    
	// Bucle principal
	while(tecla != 'q' && tecla != 'Q')
	{
		//Lectura de tecla
		tecla = getch();

		//--------------CTRL DE MOVIMIENTOS
		devPedal.Calibrate(true); //TODO Ejemplo de activación de calibración

		//Comunicación con periférico
		devPedal.Update(tiempo);

		//Set de valor
		gauPedal.SetAngle(12.5); //TODO Ejemplo 12.5 deg
		gauPedal.SetSpeedLimit(24.3);//TODO Ejemplo 24.3 deg

		//------------DIBUJO
		devPedal.Print(); //Representación del buffer recibido
		gauPedal.Draw(); //Representación del gauge

		//NO borrar esta linea. Necesaria para limpiar consola cada ciclosystem
		move(0,0);
		refresh();
		//Espera de 100ms para dibujo
	    napms(100);
	}
    
    // Desinicialización de ncurses
	endwin();

    printf("Fin del programa\n");
    return 0;
}

//Hilo de control temporal
void* hilo_tiempo(void* d)
{
	while(tecla != 'q' || tecla != 'Q')
	{
		//Incrementos de 50ms
		usleep(50000);
		tiempo+=0.05;
	}
}
