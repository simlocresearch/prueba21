# PRUEBA DE ACCESO SIMLOC 

Bienvenido a la prueba práctica de C++ de Simloc. Lee cuidadosamente este documento antes de empezar. 

El objetivo del siguiente ejercicio es el de evaluar los conocimientos y destrezas del candidato a la hora de afrontar el diseño y desarrollo de una solución de programación en un plazo de tiempo determinado. 

El lenguaje de programación que debes emplear en toda la prueba es C++. En el ejercicio puedes crear las clases o funciones que consideres necesarias para su realización. Mantener un estilo limpio, ordenado y con suficientes comentarios se valorará muy positivamente. 

Dependiendo del resultado de esta prueba, puedes ser o no seleccionado para realizar una entrevista en la sede de nuestra empresa. En ambos casos contactaremos contigo y te informaremos. Si eres seleccionado, acordaremos fecha y hora para realizar la entrevista .

Estimo que el tiempo de realización de la prueba completa es de unas 4h. 

# Entorno de desarrollo 

Se necesita una máquina Linux con un compilador GCC.

Para obtener el código, necesitarás tener instalado el programa Git y la herramienta de compilación Cmake y g++. Desde un terminal teclea:

sudo apt-get update

sudo apt-get install g++

sudo apt-get install cmake

sudo apt-get install ncurses-dev

, accede a tu carpeta de trabajo y escribe: 

git clone https://SRandres@bitbucket.org/simlocresearch/prueba21.git

Git automáticamente descargará el proyecto en la carpeta actual. Ya lo tienes todo listo para empezar. Para probar que todo funciona correctamente ejecuta el script ./build.sh 
ubicado en la carpeta /src. El programa se ejecutará en el terminal y podrás ver cómo funciona. Pulsa Q para salir de él.  Puedes hacer tus propias modificaciones y compilar de nuevo el programa con el script ./build.sh  

Ahora debes verificar la instalación de ciertas dependencias, para ello ejecuta:

sudo apt-get update
sudo apt-get install g++
sudo apt-get install cmake
sudo apt-get install ncurses-dev

Para comprobar que todo funciona correctamente, dirígete al directorio /src escribe: ./build.sh

Juega un poco con el programa para ver cómo funciona. Pulsa Q en cualquier momento para salir. 

Puedes hacer tus propias modificaciones y compilar de nuevo el programa con el script ./build.sh antes mencionado.


# Enunciado de la prueba

Un cliente nos solicita una mejora para uno de los simulador de A320 que le hemos desarrollado: Sistema de pedales y control de timón direccional

La solución consiste en incorporar un sistema de pedales en el simulador, de forma que pueda utilizar el timón direccional  y así poder manejar la guiñada del avión. Solo le interesa de los pedales la capacidad de guiñar, no le interesa de momento el sistema de frenada. 

Los compañeros del departamento de hardware han desarrollado unos pedales y el firmware asociado para poder leer la posición del balancín.  El 100% del recorrido de los pedales debe de ser útil, por lo que será necesario realizar una calibración del periférico y linealizar la señal de entrada a un rango de trabajo cómodo.

El cliente nos ha proporcionado documentación oficial del avión sobre la superficie de control en cuestión:

-El timón direccional del avión tiene un fondo de escala mecánico de 25º en ambos sentidos.
-La tarjeta electrónica que maneja el timón, tiene una rutina de seguridad basada en un sistema de control realimentado  con el valor de velocidad de la aeronave, donde en 160 nudos el valor MAX es de 25º y a 380 nudos de 3.4º

Al no disponer de mayor detalle de la curva, se ha acordado con el cliente linealizar la ecuación a partir de los dos puntos conocidos. El criterio de signos es de pisada derecha incremento positivo de ángulo, y pisada izquierda incremento negativo de ángulo.

En cuanto al modelo matemático que controla el actuador de la superficie de control, el cliente no ha podido proporcionarnos gran información, únicamente decirnos que el tiempo que tarda el timón en alcanzar una posición determinada tras aplicar una señal en los pedales es de unos 2 seg, y que la respuesta a la consigna de los pedales sigue una tendencia exponencial, desplazándose el timón más despacio a medida que se aproxima al valor final determinado por la posición del balancín.

El gauge que representa la posición de la superficie ya está desarrollado, pero falta desarrollar la interpretación de la señal del periférico y el algoritmo de control como se ha mencionado.

# Notas para la realización de la prueba

La información del sensor que registra el ángulo de giro de los pedales se nos proporciona en una trama HID con la siguiente estructura:

Byte 0 Tamaño trama
Byte 1 Cod. Seguridad
Byte 2 0
Byte 3 Byte alto sensor
Byte 4 Byte bajo sensor

El primer byte indica el tamaño del mensaje (siempre deberá ser 5).
El segundo byte vale 0x80 si el mensaje se ha enviado de forma correcta. Cualquier otro valor no garantiza la integridad del mensaje.
El tercer byte vale 0, previsto para futuras ampliaciones del periférico.
El cuarto y quinto byte contienen los datos del sensor, por lo que el valor de la medida estará comprendido entre 0000 y FFFF (el pedal solo usará una parte de este rango ya que el balancín no realiza un giro completo de 360º).

Los valores de las señales se encuentra en sistema hexadecimal, por lo que tendrás que realizar las conversiones oportunas para poder trabajar cómodamente con estos datos.

Para tener acceso a esta lectura utilizamos la clase PedalDevice, la cual tiene varios métodos interesantes de utilizar:

void Update(): El puerto de comunicación se mantiene a la escucha del periférico. Esta función debe llamarse cíclicamente si se quiere recibir mensajes del dispositivo.
void Print(): Se imprime por consola la última trama HID recibida en la lectura del periférico
void GetBuffer(byte[ ] ): Se copia por referencia la última trama HID recibida en la lectura del periférico.

Por otro lado, dado que no dispones de los pedales físicos, hay un par de métodos que emulan la comunicación con el dispositivo:

void Calibrate(bool state): Al pasar un valor de true  por parámetro se activa la simulación de un movimiento de calibración en el periférico. Este consiste en partir de la posición central, esperar 3seg, avanzar hacia la máxima posición con el pie derecho, esperar 3 seg, avanzar hacia la máxima posición con el pie izquierdo, esperar 3 seg, y retornar al centro. La duración de la simulación es de unos 12 seg aproximadamente, considerando los 3 seg de las esperas y el tiempo invertido en realizar los recorridos (1 seg aproximadamente por recorrido).
void Simulate(bool state): El departamento de hardware ha realizado un ensayo con el cliente y los pedales. El cliente (y además piloto) ha realizado una serie de movimientos en los pedales asociados a un procedimiento específico del vuelo. Los compañeros de hardware han grabado el ejercicio (unos 20 seg) y las señales del sensor durante todo el proceso. Al igual que el método Calibrate, este método genera la simulación de esos movimientos.

Los dos métodos se activan por flanco de subida, quiere decir que si activas una simulación y esta finaliza, para poder volver a lanzarla debes generar una caída de flanco en la variable state para poder volver a realizarla. 

Consejo: La señal de un eje de joystick, una vez convertida, suele utilizar el rango de -1.0 a 1.0 o de -100.0 a 100.0, siendo 0.0 la posición central. Con cualquiera de estos dos rangos de trabajo es bastante cómodo vincular el desplazamiento de un mando de vuelo a una consigna de trabajo para un actuador.

Para la función de transferencia entre la señal de los pedales y la posición del timón vamos a reutilizar la clase Modelo desarrollada por el departamento, basada en sistemas LTI de primer orden. Su contenido está basado en una ecuación diferencial moldeable, pero lo único que necesitas saber es que su atributo característico es la cte de Tiempo que determina la velocidad del sistema, y que su valor representa el tiempo τ en segundos que tarda el sistema en alcanzar el 63,2% del valor final de la señal. Por otro lado, se asume la aproximación de que el régimen permanente (95% del valor final) se obtiene transcurrido 3τ segundos. Los principales métodos son:

Modelo(float cteT): El constructor recibe el valor del polo que caracteriza el modelo.
void StartModelo(float _t, ...): Establece el valor To del sistema. Esta función deberá llamarse una única vez justo antes de comenzar la etapa de escritura en el actuador del timón direccional. El resto de parámetros deben valer 0 en la llamada.
bool ModeloStarted(): Devuelve estado de iniciación del modelo.
void Update(float t): El sistema calcula el valor de la señal asociado al instante de tiempo. El modelo utiliza la diferencia temporal entre el tiempo proporcionado en la llamada y el To fijado previamente. Esta función deberá llamarse cíclicamente mientras dure el control del actuador.
void Check_K(float _t, float new_k): El sistema recibe el valor consigna a seguir y el instante de tiempo actual. Esta función deberá llamarse cíclicamente mientras dure el control del actuador.
float GetValue(): El modelo devuelve el último valor de la ecuación diferencial calculado.

La clase Gauge permite representar el valor del ángulo de giro del timón y el limitador de recorrido por velocidad en un instrumento digital. Los métodos principales son:

SetAngle(floar ): Recibe en grados el ángulo de giro.
SetSpeedLimit(float ): Recibe en grados el ángulo límite de giro.
Draw(): Representa el instrumento de vuelo

ANTES DE COMENZAR REVISA LAS CABECERAS DE TODAS LAS CLASES PARA TENER TODA LA INFORMACIÓN NECESARIA SOBRE LOS MÉTODOS PÚBLICOS.

# Entrega de la solución

Para completar el ejercicio deben realizarse los siguientes apartados:
TAREA DE DISEÑO
Diagrama de bloques de la señal del pedal, desde la lectura en bruto del pedal hasta el valor aplicado en el actuador. 
Máquina de estados o Grafcet de la solución, desde la etapa inicial de recibir la trama del pedal hasta la escritura en el gauge. 
TAREAS DE PROGRAMACIÓN
Función o clase de calibración del periférico
Función o clase de limitación del recorrido del timón
Código responsable de la función de transferencia entre la señal en bruto del sensor del pedal y la representación de movimiento en grados del timón en el gauge. Para ello el programa debe representar gráficamente los movimientos registrados por el cliente en el método Simulate(), y considerando que el avión se desplaza a 280 kts de velocidad.
Si no te da tiempo a resolver el ejercicio por completo antes de que finalice el plazo, envíame igualmente lo que tengas, aunque esté incompleto.
En el caso de que completes todos los puntos y tengas ganas de continuar, te propongo la siguiente mejora que seguro que el cliente deseará:
Sistema de ficheros para calibraciones: Con la solución que has desarrollado se requiere de la calibración del pedal en cada arranque del simulador. Sería interesante poder guardar los parámetros fundamentales de la calibración en un fichero de extensión .cfg, y que en cada arranque el sistema verifique el contenido de ese fichero en lugar de tener que repetir la calibración.
Si utilizas Codeanywhere, utiliza la opción Download clickando con el botón derecho sobre SRPueba18. Obtendrás un fichero comprimido con tu proyecto, envíamelo por email a andres.barroso@simloc.es
Si no usas Codeanywhere, archiva tu proyecto en un fichero .tar.gz o .zip y envíamelo por email a andres.barroso@simloc.es
Cualquier anotación o comentario que quieras realizar sobre tu proyecto incluyelo en el email.

¡Buena suerte!