//Clase responsable de la comunicación con el periférico,
//no es necesaria su modificación para la prueba
#include <stdio.h>
#include <curses.h>
#include <string.h>

#define SIZE_BUFFER 5 //Tamaño del buffer de comunicación

//Clase responsable de la comunicación con el periférico del Pedal
class PedalDevice
{
  
  private:  
    unsigned char m_buffer[SIZE_BUFFER]; //Trama de lectura del dispositivo
    bool m_calibrating; //Estado activo de calibración
    bool m_simulating; //Estado activo de simulación
    bool m_finish_calibrate; //Estado de fin de movimientos de calibración
    bool m_finish_simulate; //Estado de fin de movimientos de simulación

    double m_tiempo; //Valor de tiempo de simulación
    double m_tiempo0; //Tiempo inicio simulación
    
  public:
  
    PedalDevice();
    ~ PedalDevice();
    
    //--ACCESO AL ESTADO DE MOVIMIENTOS
    bool isCalibrating();
    bool isSimulating();
    bool isFinishCalibrate();
    bool isFinishSimulate();

    //--COMUNICACIÓN CON PERIFÉRICO
    void Update(double tiempo); //Escucha del puerto serie del dispositivo
    void GetBuffer(unsigned char buffer[]); //Paso por referencia del buffer

    //--ACTIVACIÓN DE MODOS DE MOVIMIENTO
    void Calibrate(bool state); //Simulación de movimientos de calibración
    void Simulate(bool state); //Simulación de movimientos del cliente

    //--SALIDA POR PANTALLA
    void Print(); //Salida por pantalla del buffer
};
