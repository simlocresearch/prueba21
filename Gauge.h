#include <stdio.h>
#include <curses.h>

//Clase responsable de representar un gauge de instrumento
class Gauge{

	private:
		float m_angle; //Ángulo de timón en grados
		float m_speed; //Ángulo límite del timón en grados
		float m_aux; //Variable auxiliar de representación
		bool m_calibrating; //Modo CALIBRACIÓN
		bool m_simulating; //Modo SIMULACIÓN

	public:
		Gauge();

		void SetAngle(float ); //Setea el valor del timón en grados
		void SetSpeedLimit(float ); //Setea el valor límite del timón en grados

		//--SALIDA POR PANTALLA
		void SetAux(float aux); //Variable auxiliar
		void SetMode(char mode); //'C': Calibrating 'S':Simulating
		void Draw(); //Representación de gauge
};
